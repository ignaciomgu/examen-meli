
# Sistema Informático para predecir el tiempo en una galaxia lejana

Examen de Ingreso Mercado Libre

## Comenzando

Por medio de estas instrucciones vas a poder: Clonar el proyecto, Ejecutarlo en algún IDE y Probar su API tanto Cloud como Local.

### Prerequisitos

Que cosas necesitas para instalarlo

```
Algún IDE como Intellij o Eclipse (recomiendo el primero)
JDK 8
Maven
```

### Instalación


Primero clonar el repo:

```
Via SSH (vas a tener que crear y agregar previamente el par de claves): 
Abrir consola -> git clone git@gitlab.com:ignaciomgu/examen-meli.git

o

Via HTTPS (directo): 
Abrir consola -> git clone https://gitlab.com/ignaciomgu/examen-meli.git
```

Una vez descargado el código ir dentro de la carpeta en donde se descargó el código se deberá realizar:

Ejecutar Cobertura:
```
Abrir consola -> mvn cobertura:cobertura
```
Va a correr los tests y generar un reporte en /target/site/cobertura/index.html

Ejecutar Maven:
```
Abrir consola -> mvn clean install -U
```
Allí se realizará la fase de limpieza en cada módulo antes de ejecutar la fase de instalación para cada módulo, luego correrá los tests.

## Reporte de  cobertura de tests

Como se visualiza en el reporte arrojado por Cobertura, se cubrio casi el 100 porciento del codigo.

<img src="https://gitlab.com/ignaciomgu/examen-meli/raw/master/src/main/resources/CoberturaCapture.PNG">

## Diagrama de Clases

En este diagrama de clases esta plasmada la logica del modelo de negocios, no así la logica de carga y almacenamiento masivo de los datos del pronostico, las clases
de configuración, ni de autenticación (ya que en esta utlima se hace a modo demostrativo con usuarios en memoria).
Este proporciona una idea mínima y estática del diseño del sistema para hacer el entendimiento mas ameno del proyecto

<img src="https://gitlab.com/ignaciomgu/examen-meli/raw/master/src/main/resources/DC.png">

## Corriendo el servidor

Vamos a la clase "MeliGalaxyApplication" -> click derecho -> Run.
Este paso levantará un tomcat embebido y luego estrá corriendo en http://localhost:8080


## Ruta API Rest Local: trae todo el pronostico hasta el día 3650

GET: http://localhost:8080/api/pronostico?dia=3650
```
Con usuario: meli
```
```
Con password: iamin
```
```
Ejemplo: curl localhost:8080/api/clima?dia=566 -u meli:iamin
```

## Ruta API Rest Local: trae el pronostico del día 566

GET: http://localhost:8080/api/clima?dia=566
```
Con usuario: meli
```
```
Con password: iamin
```
```
Ejemplo: curl localhost:8080/api/pronostico?dia=3650 -u meli:iamin
```

## Ruta API Rest Cloud: trae todo el pronostico hasta el día 3650

GET: http://examen-meli-2019.herokuapp.com/api/pronostico?dia=3650
(Sin user ni password)
```
Ejemplo: curl examen-meli-2019.herokuapp.com/api/pronostico?dia=3650 
```
(Esta request puede tardar aproximadamente 20s)

## Ruta API Rest Cloud: trae el pronostico del día 566

GET: http://examen-meli-2019.herokuapp.com/api/clima?dia=566
(Sin user ni password)
```
Ejemplo: curl examen-meli-2019.herokuapp.com/api/clima?dia=566 
```
(Esta request puede tardar aproximadamente 20s)

## Supuestos del Enunciado

* Los planetas comienzan en la posicion donde 'X' es igual a la distancia al sol en Km e 'Y' es 0.0
* Los planetas están alineados, si y solo sí lo están con respecto a su eje, es decir, sin indice de tolerancia y/o volumen. No obstante, se agregó un método que soporta
índice de tolerancia en la clasee 'AlignmentCalculator', con el fin de obtener mejores resultados. Si bién este método no se está utilizando en producción, el
mismo se encuentra incluido en la batería de tests.
* El año posee 365 días, por lo tanto la posición de cada planeta en el día 1 será el mismo que en el día 366
* El sistema soporta solo 3 planetas y un sol, se asume que no aparecerá otro mas. Mas que nada por cuestiones de no sobregeneralizar el modelo, por ejemplo:
en un juego de ajedréz existen 2 jugadores, y solo va a haber 2 jugadores, no necesito utilizar un array de jugadores ya que siempre habrá la misma cantidad.
* La versión cloud no soporta autenticación de usuarios. La versión subida a git si posee con una implementación InMemory básica a modo de prototipo experimental.

## Construido Con

* [HQ2Database embebida](http://www.h2database.com/html/main.html) - La Web de la base de datos
* [Maven](https://maven.apache.org/) - La Web del Administrador de Dependencias
* [Spring Boot](https://spring.io/projects/spring-boot) - La Web del Framework 
* [Springfox Swagger2](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api) - (Configurado sencillamente)
* [Spring Security](https://spring.io/projects/spring-security) - (Configurado sencillamente)
* [Cobertura](https://www.baeldung.com/cobertura) - Web de la librería para reportes de cobertura de testing de código
* [JUnit](https://junit.org/junit4/) - Web de la libreria de Testing
* [SLF4J](https://www.slf4j.org/) - Web de la librería de Logging

## Versionado

Utilizando [GitLab](https://gitlab.com/ignaciomgu/examen-meli) para versionado. 

## Autor

* **Ignacio Gallardo** - [CV](https://ignaciogallardocv.herokuapp.com/)


package galaxy.weatherforecast.api.model;
import static org.assertj.core.api.Assertions.assertThat;
import com.google.gson.Gson;
import galaxy.weatherforecast.api.model.dto.ErrorResponseDTO;
import galaxy.weatherforecast.api.model.dto.WeatherDescriptorDTO;
import galaxy.weatherforecast.api.model.dto.WeatherForecastDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WeatherForecastControllerTest {

    @Autowired
    private WeatherForecastDescriptorService weatherForecastDescriptorService;

    @LocalServerPort
    private int port;

    private final String host = "http://localhost:";

    private final String pathForecast = "/api/pronostico?dia=";

    private final String hostWeather = "/api/clima?dia=";

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Prueba el resultado exitoso que arroja la API de pronostico en un hasta un dia determinado
     * @throws Exception
     */
    @Test
    public void getForecasteOkApiTest() throws Exception {
        int day = 3650;
        WeatherForecastDTO weatherForecastDTO = weatherForecastDescriptorService.getForecast(day);
        final Gson gson = new Gson();
        final String weatherForecastDTOJson = gson.toJson(weatherForecastDTO);
        assertThat(this.restTemplate.getForObject(host + port + pathForecast + day,
                String.class)).contains(weatherForecastDTOJson);
    }

    /**
     * Prueba el resultado que arroja la API de pronostico al exponer casos de no uso
     * @throws Exception
     */
    @Test
    public void getForecasteBadApiTest() throws Exception {
        int day = 3651;
        int day2 = -1;
        String day3 = "asd";
        final Gson gson = new Gson();
        final String error = gson.toJson(new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650"));
        assertThat(this.restTemplate.getForObject(host + port + pathForecast + day,
                String.class)).contains(error);
        assertThat(this.restTemplate.getForObject(host + port + pathForecast + day2,
                String.class)).contains(error);
        assertThat(this.restTemplate.getForObject(host + port +pathForecast + day3,
                String.class)).contains(error);
    }

    /**
     * Prueba el resultado exitoso que arroja la API de clima en un dia determinado
     * @throws Exception
     */
    @Test
    public void getWeatherOkApiTest() throws Exception {
        int day = 566;
        WeatherDescriptorDTO weatherDescriptorDTO = weatherForecastDescriptorService.getWeather(day);
        final Gson gson = new Gson();
        final String weatherDescriptorDTOOJson = gson.toJson(weatherDescriptorDTO);
        assertThat(this.restTemplate.getForObject(host + port + hostWeather + day,
                String.class)).contains(weatherDescriptorDTOOJson);
    }

    /**
     * Prueba el resultado que arroja la API del clima al exponer casos de no uso
     * @throws Exception
     */
    @Test
    public void getWeatherBadApiTest() throws Exception {
        int day = 3651;
        int day2 = -1;
        String day3 = "asd";
        final Gson gson = new Gson();
        final String error = gson.toJson(new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650"));
        assertThat(this.restTemplate.getForObject(host + port + hostWeather + day,
                String.class)).contains(error);
        assertThat(this.restTemplate.getForObject(host + port + hostWeather + day2,
                String.class)).contains(error);
        assertThat(this.restTemplate.getForObject(host + port + hostWeather + day3,
                String.class)).contains(error);
    }


}

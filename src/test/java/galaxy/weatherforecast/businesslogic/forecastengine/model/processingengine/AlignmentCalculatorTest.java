package galaxy.weatherforecast.businesslogic.forecastengine.model.processingengine;

import galaxy.weatherforecast.businesslogic.forecastengine.model.AlignmentCalculator;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class AlignmentCalculatorTest {

    /**
     * Prueba que cuatro puntos esten alineados (los tres planetas y el Sol) 2 Ejemplos dentro del test
     */
    @Test
    public void alignmentOfFourPoints() {
        assertThat(AlignmentCalculator.areAlignedAndWithTheSun(2,0,5,0,10,0)).isTrue();
        assertThat(AlignmentCalculator.areAlignedAndWithTheSun(0,2,0,5,0,10)).isTrue();
    }

    /**
     * Prueba que cuatro puntos no esten alineados (los tres planetas y el Sol) 2 Ejemplos dentro del test
     */
    @Test
    public void notAlignmentOfFourPoints() {
        assertThat(AlignmentCalculator.areAlignedAndWithTheSun(2,1,5,0,10,0)).isFalse();
        assertThat(AlignmentCalculator.areAlignedAndWithTheSun(0,2,0,5,2,10)).isFalse();
    }

    /**
     * Prueba que tres puntos esten alineados y no esten alineados con otro punto (los tres planetas y el Sol)
     */
    @Test
    public void alignmentOfThreePointsAndWithNotWithTheFourth() {
        assertThat(AlignmentCalculator.areAlignedButNotWithTheSun(0,5000,3000,3000,7500,0)).isTrue();
    }

    /**
     * Prueba que tres puntos no esten alineados y no esten alineados con otro punto (los tres planetas y el Sol)
     */
    @Test
    public void notAlignmentOfThreePointsAndWithNotWithTheFourth() {
        assertThat(AlignmentCalculator.areAlignedButNotWithTheSun(0,5,3,3,0,7500)).isFalse();
    }

    /**
     * Prueba que cuatro puntos estén alineados con un indice de tolerancia (utilizando el metodo de indice de tolerancia)
     * Y también prueba que esos mismos puntos den como que no estén alineados utilizando el metodo sin indice de tolerancia
     */
    @Test
    public void alignmentOfFourPointsWithTolerance() {
        //Debe ser falso, por que utiliza el metodo sin indice de tolerancia
        assertThat(AlignmentCalculator.areAlignedAndWithTheSun(1,1,2,2,3,4)).isFalse();
        //Debe ser verdadero, por que el error es -+ el indice de tolerancia
        assertThat(AlignmentCalculator.areAlignedAndWithTheSunWithTolerance(1,1,2,2,3,4)).isTrue();
    }

    /**
     * Prueba que cuatro puntos no esten alineados utilizando el indice de tolerancia, por que el error se pasa de dicho indice
     */
    @Test
    public void NotAlignmentOfFourPointsWithTolerance() {
        //Debe ser falseo, por que el error se pasa del indice de tolerancia
        assertThat(AlignmentCalculator.areAlignedAndWithTheSunWithTolerance(1,1,2,1,3,4)).isFalse();

    }

    /**
     * Prueba que tres puntos esten alineados con una tolerancia y no esten alineados con otro punto (los tres planetas y el Sol)
     */
    @Test
    public void alignmentOfThreePointsAndWithNotWithTheFourthWithTolerance() {
        //Debe ser verdadero, por que el error es -+ el indice de tolerancia entre los planetas, pero excede el indice con respecto al sol
        assertThat(AlignmentCalculator.areAlignedButNotWithTheSunWithTolerance(1,2.6,2,3.6,3,4)).isTrue();
    }

}

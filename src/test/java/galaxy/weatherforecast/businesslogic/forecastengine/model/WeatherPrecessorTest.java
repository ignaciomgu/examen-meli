package galaxy.weatherforecast.businesslogic.forecastengine.model;

import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBodyFactory;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import org.junit.Test;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherProcessor;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class WeatherPrecessorTest {

    private WeatherCalculator weatherCalculator;

    /**
     * Prueba el valor por defecto que toma el tiempo al no ser lluvioso, seco u optimo
     */
    @Test
    public void defaultValuesWeatherProcessor() {
        WeatherProcessor weatherResolver = mock(WeatherProcessor.class);
        when(weatherResolver.isOccurring(any(SolarSystem.class))).thenReturn(false);
        weatherCalculator = new WeatherCalculator(Collections.<WeatherProcessor>singletonList(weatherResolver));
        WeatherDescriptor weatherInfo = weatherCalculator.calculate(mock(SolarSystem.class));
        assertThat(weatherInfo.isRainyDay()).isFalse();
        assertThat(weatherInfo.isDroughtDay()).isFalse();
        assertThat(weatherInfo.isOptimalDay()).isFalse();

    }

    /**
     * Prueba el correcto calculo de un día Optimo. Mockeando el porcesador de clima
     */
    @Test
    public void optimalMatcherWeatherProcessor() {
        SolarSystem stellarSystem = new SolarSystem(CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide());
        WeatherProcessor weatherResolver = mock(WeatherProcessor.class);
        when(weatherResolver.isOccurring(any(SolarSystem.class))).thenReturn(true);
        when(weatherResolver.create(stellarSystem))
                .thenReturn(WeatherDescriptor.createOptimal(stellarSystem));
        weatherCalculator = new WeatherCalculator(Collections.singletonList(weatherResolver));
        WeatherDescriptor weatherInfo = weatherCalculator.calculate(stellarSystem);
        assertThat(weatherInfo.isOptimalDay()).isTrue();
        assertThat(weatherInfo.isDroughtDay()).isFalse();
        assertThat(weatherInfo.isRainyDay()).isFalse();
    }

    /**
     * Prueba el correcto cálculo de un dia lluvioso.  Mockeando el porcesador de clima
     */
    @Test
    public void rainyMatcherWeatherProcessor() {
        SolarSystem stellarSystem = new SolarSystem(CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide());
        WeatherProcessor weatherResolver = mock(WeatherProcessor.class);
        when(weatherResolver.isOccurring(any(SolarSystem.class))).thenReturn(true);
        when(weatherResolver.create(stellarSystem))
                .thenReturn(WeatherDescriptor.createRain(stellarSystem,1.0));
        weatherCalculator = new WeatherCalculator(Collections.singletonList(weatherResolver));
        WeatherDescriptor weatherInfo = weatherCalculator.calculate(stellarSystem);
        assertThat(weatherInfo.isOptimalDay()).isFalse();
        assertThat(weatherInfo.isDroughtDay()).isFalse();
        assertThat(weatherInfo.isRainyDay()).isTrue();
    }

    /**
     * Crea el correcto calculo de un dia seco. Mockeando el porcesador de clima
     */
    @Test
    public void droughtMatcherWeatherProcessor() {
        SolarSystem stellarSystem = new SolarSystem(CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide(),
                CelestialBodyFactory.createBetasoide());
        WeatherProcessor weatherResolver = mock(WeatherProcessor.class);
        when(weatherResolver.isOccurring(any(SolarSystem.class))).thenReturn(true);
        when(weatherResolver.create(stellarSystem))
                .thenReturn(WeatherDescriptor.createDrought(stellarSystem));
        weatherCalculator = new WeatherCalculator(Collections.singletonList(weatherResolver));
        WeatherDescriptor weatherInfo = weatherCalculator.calculate(stellarSystem);
        assertThat(weatherInfo.isOptimalDay()).isFalse();
        assertThat(weatherInfo.isDroughtDay()).isTrue();
        assertThat(weatherInfo.isRainyDay()).isFalse();
    }
}


package galaxy.weatherforecast.businesslogic.forecastengine.model.processingengine;

import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBody;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.DroughtDayProcessor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.OptimalDayProcessor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.RainyDayProcessor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PeriodsProcessorTest {

  private DroughtDayProcessor droughtDayProcessor;
  private RainyDayProcessor rainyDayProcessor;
  private OptimalDayProcessor optimalDayProcessor;

  @Before
  public void setup() {
    droughtDayProcessor = new DroughtDayProcessor();
    rainyDayProcessor = new RainyDayProcessor();
    optimalDayProcessor = new OptimalDayProcessor();
  }

  /**
   * Se fuerzan los valores para crear un dia seco con exito
   */
  @Test
  public void theDroughtPeriodIsHappening() {
    CelestialBody firstCelestialBody = new CelestialBody("firstCelestialBody", 1, 200);
    CelestialBody secondCelestialBody = new CelestialBody("secondCelestialBody", 1, 500);
    CelestialBody thirdCelestialBody = new CelestialBody("thirdCelestialBody", 1, 1000);

    SolarSystem solarSystem = new SolarSystem(firstCelestialBody, secondCelestialBody, thirdCelestialBody);

    firstCelestialBody.doRegression(3600);
    secondCelestialBody.doRegression(3600);
    thirdCelestialBody.doRegression(3600);

    assertThat(droughtDayProcessor.isOccurring(solarSystem)).isTrue();

    WeatherDescriptor weatherDescriptor = droughtDayProcessor.create(solarSystem);
    assertThat(weatherDescriptor).isNotNull();
    assertThat(weatherDescriptor.isDroughtDay()).isTrue();
    assertThat(weatherDescriptor.isRainyDay()).isFalse();
    assertThat(weatherDescriptor.isOptimalDay()).isFalse();

  }

  /**
   * Se fuerzan los valores para crear un dia lluvioso con exito
   */
  @Test
  public void theRainyPeriodIsHappening() {
    //Sol dentro del triangulo que forman los tres planetas
    CelestialBody firstCelestialBody = new CelestialBody("firstCelestialBody", 1, 100);
    CelestialBody secondCelestialBody = new CelestialBody("secondCelestialBody", 2, 300);
    CelestialBody thirdCelestialBody = new CelestialBody("thirdCelestialBody", -5, 500);
    SolarSystem solarSystem = new SolarSystem(firstCelestialBody, secondCelestialBody, thirdCelestialBody);

    firstCelestialBody.doRegression(45);
    secondCelestialBody.doRegression(70);
    thirdCelestialBody.doRegression(10);

    assertThat(rainyDayProcessor.isOccurring(solarSystem)).isTrue();

    WeatherDescriptor weatherDescriptor = rainyDayProcessor.create(solarSystem);
    assertThat(weatherDescriptor).isNotNull();
    assertThat(weatherDescriptor.isDroughtDay()).isFalse();
    assertThat(weatherDescriptor.isRainyDay()).isTrue();
    assertThat(weatherDescriptor.isOptimalDay()).isFalse();

  }

  /**
   * Se fuerzan los valores para crear un dia optimpo con exito
   */
  @Test
  public void theOptimalPeriodIsHappening() {
    //Planetas alineados entre si pero no con el sol
    CelestialBody firstCelestialBody = new CelestialBody("firstCelestialBody", 90, 5000);
    CelestialBody secondCelestialBody = new CelestialBody("secondCelestialBody", 45, 4243);
    CelestialBody thirdCelestialBody = new CelestialBody("thirdCelestialBody", 0, 7500);
    SolarSystem solarSystem = new SolarSystem(firstCelestialBody, secondCelestialBody, thirdCelestialBody);

    firstCelestialBody.doRegression(1);
    secondCelestialBody.doRegression(1);
    thirdCelestialBody.doRegression(1);

    assertThat(optimalDayProcessor.isOccurring(solarSystem)).isTrue();

    WeatherDescriptor weatherDescriptor = optimalDayProcessor.create(solarSystem);
    assertThat(weatherDescriptor).isNotNull();
    assertThat(weatherDescriptor.isDroughtDay()).isFalse();
    assertThat(weatherDescriptor.isRainyDay()).isFalse();
    assertThat(weatherDescriptor.isOptimalDay()).isTrue();
  }

}
package galaxy.weatherforecast.businesslogic.weatherforecast.model.service;

import galaxy.weatherforecast.api.model.WeatherForecastDescriptorService;
import galaxy.weatherforecast.api.model.dto.WeatherDescriptorDTO;
import galaxy.weatherforecast.api.model.dto.WeatherForecastDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherForecastServiceImplTest {

    @Autowired
    private WeatherForecastDescriptorService weatherForecastDescriptorService;

    @Test
    public void completeCyclePersistenceGettingForecastOfTenYearTest() {
        WeatherForecastDTO weatherForecastDTO = weatherForecastDescriptorService.getForecast(3650);
        assertThat(weatherForecastDTO.getDiasMasLLuviosos().size()).isEqualTo(50);
        assertThat(weatherForecastDTO.getTotalDeDiasLLuviosos()).isEqualTo(2420);
        assertThat(weatherForecastDTO.getTotalDeDiasOptimos()).isEqualTo(0);
        assertThat(weatherForecastDTO.getTotalDeDiasSecos()).isEqualTo(0);
    }

    @Test
    public void completeCyclePersistenceGettingWeatherOfASpecificDayYearTest() {
        WeatherDescriptorDTO weatherDescriptorDTO = weatherForecastDescriptorService.getWeather(566);
        assertThat(weatherDescriptorDTO.getLluvia()).isEqualTo(6003.851703584671);
        assertThat(weatherDescriptorDTO.getDia()).isEqualTo(566);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteA().getGrados()).isEqualTo(243);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteB().getGrados()).isEqualTo(201);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteC().getGrados()).isEqualTo(75);

        weatherDescriptorDTO = weatherForecastDescriptorService.getWeather(567);
        assertThat(weatherDescriptorDTO.getLluvia()).isEqualTo(6060.664369842538);
        assertThat(weatherDescriptorDTO.getDia()).isEqualTo(567);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteA().getGrados()).isEqualTo(246);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteB().getGrados()).isEqualTo(202);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteC().getGrados()).isEqualTo(70);

        weatherDescriptorDTO = weatherForecastDescriptorService.getWeather(800);
        assertThat(weatherDescriptorDTO.getLluvia()).isEqualTo(6230.58238795523);
        assertThat(weatherDescriptorDTO.getDia()).isEqualTo(800);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteA().getPosicion().getX()).isEqualTo(-1732.0);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteA().getPosicion().getY()).isEqualTo(-1000.0);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteB().getPosicion().getX()).isEqualTo(171.0);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteB().getPosicion().getY()).isEqualTo(470.0);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteC().getPosicion().getX()).isEqualTo(985.0);
        assertThat(weatherDescriptorDTO.getSistemaSolarDTO().getCuerpoCelesteC().getPosicion().getY()).isEqualTo(174.0);

    }
}

package galaxy.weatherforecast.businesslogic.weatherforecast.model.repository;


import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBody;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherCalculator;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherProcessor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WeatherForecastRepositoryTest {

    @Autowired
    private WeatherForecastRepository weatherForecastRepository;

    /**
     * Prueba la persistencia del clima
     */
    @Test
    public void completeCyclePersistenceTest() {
        CelestialBody celestialBody1 = new CelestialBody("startrek", 1, 1);
        CelestialBody celestialBody2 = new CelestialBody("startrek", 1, 1);
        CelestialBody celestialBody3 = new CelestialBody("startrek", 1, 1);
        SolarSystem stellarSystem = new SolarSystem(celestialBody1, celestialBody2, celestialBody3);
        WeatherProcessor processor = mock(WeatherProcessor.class);
        when(processor.isOccurring(stellarSystem)).thenReturn(true);
        AtomicInteger precipitation = new AtomicInteger(1);
        when(processor.create(stellarSystem))
                .thenAnswer(
                        invocation -> WeatherDescriptor.createRain(stellarSystem, precipitation.getAndIncrement()));
        WeatherCalculator weatherCalculator = new WeatherCalculator(
                Collections.singletonList(processor));
        weatherForecastRepository = new WeatherForecastRepository(stellarSystem, weatherCalculator);
        assertThat(weatherForecastRepository.findBy(366).getRain()).isEqualTo(1.0);
        assertThat(weatherForecastRepository.findBy(367).getRain()).isEqualTo(2.0);
        assertThat(weatherForecastRepository.findBy(368).getRain()).isEqualTo(3.0);
    }

}

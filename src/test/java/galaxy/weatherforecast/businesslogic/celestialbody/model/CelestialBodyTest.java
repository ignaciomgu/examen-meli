package galaxy.weatherforecast.businesslogic.celestialbody.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CelestialBodyTest {

  /**
   * Prueba la regresion en distintos dias de un planeta, es decir, que valores tendra un día determinado en cuestiones
   * de rotación positiva
   */
  @Test
  public void positiveRotationTest() {

    CelestialBody celestialBody = new CelestialBody("startrek", 1, 10);

    celestialBody.doRegression(100);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(100);

    celestialBody.doRegression(200);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(200);

    celestialBody.doRegression(365);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(5);

    celestialBody.doRegression(566);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(206);

    celestialBody = new CelestialBody("startrek", 3, 100);

    celestialBody.doRegression(700);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(300);

    celestialBody.doRegression(850);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(30);

    celestialBody.doRegression(2000);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(240);

    celestialBody.doRegression(3650);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(150);
  }

  /**
   * Prueba la regresion en distintos dias de un planeta, es decir, que valores tendra un día determinado en cuestiones
   * de rotación negativa
   */
  @Test
  public void negativeRotationTest() {
    CelestialBody celestialBody = new CelestialBody("startrek", -1, 10);

    celestialBody.doRegression(100);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(260);

    celestialBody.doRegression(200);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(160);

    celestialBody.doRegression(365);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(355);

    celestialBody.doRegression(566);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(154);

    celestialBody = new CelestialBody("startrek", -3, 10);

    celestialBody.doRegression(700);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(60);

    celestialBody.doRegression(850);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(330);

    celestialBody.doRegression(2000);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(120);

    celestialBody.doRegression(3650);
    assertThat(celestialBody.getAngleInDegrees()).isEqualTo(210);
  }

  /**
   * Prueba la regresion en distintos dias de un planeta, es decir, que valores tendra un día determinado en cuestiones
   * de posición
   */
  @Test
  public void coordinatesTest() {
    CelestialBody celestialBody = new CelestialBody("startrek", 1, 1000);
    celestialBody.doRegression(20);
    assertThat(celestialBody.getX()).isEqualTo(940.0);
    assertThat(celestialBody.getY()).isEqualTo(342.0);

    celestialBody.doRegression(90);
    assertThat(celestialBody.getX()).isEqualTo(0);
    assertThat(celestialBody.getY()).isEqualTo(1000);

    celestialBody.doRegression(180);
    assertThat(celestialBody.getX()).isEqualTo(-1000);
    assertThat(celestialBody.getY()).isEqualTo(0);

    celestialBody.doRegression(270);
    assertThat(celestialBody.getX()).isEqualTo(0);
    assertThat(celestialBody.getY()).isEqualTo(-1000);

    celestialBody.doRegression(360);
    assertThat(celestialBody.getX()).isEqualTo(1000);
    assertThat(celestialBody.getY()).isZero();

    celestialBody = new CelestialBody("startrek", -5, 500);
    celestialBody.doRegression(20);
    assertThat(celestialBody.getX()).isEqualTo(-87.0);
    assertThat(celestialBody.getY()).isEqualTo(-492.0);
  }

  /**
   * Prueba la correcta clonación de un planeta
   */
  @Test
  public void cloningTest() {
    CelestialBody celestialBody = new CelestialBody("startrek", 1, 1000);
    CelestialBody cloneCelestialBody = celestialBody.getClone();

    cloneCelestialBody.doRegression(20);
    celestialBody.doRegression(20);
    assertThat(cloneCelestialBody.getX()).isEqualTo(celestialBody.getX());
    assertThat(cloneCelestialBody.getY()).isEqualTo(celestialBody.getY());

    cloneCelestialBody.doRegression(90);
    celestialBody.doRegression(90);
    assertThat(cloneCelestialBody.getX()).isEqualTo(celestialBody.getX());
    assertThat(cloneCelestialBody.getY()).isEqualTo(celestialBody.getY());

    celestialBody.doRegression(360);
    cloneCelestialBody.doRegression(360);
    assertThat(cloneCelestialBody.getX()).isEqualTo(celestialBody.getX());
    assertThat(cloneCelestialBody.getY()).isEqualTo(celestialBody.getY());

    assertThat(cloneCelestialBody.getAngleInDegrees()).isEqualTo(celestialBody.getAngleInDegrees());

    assertThat(cloneCelestialBody.getAngleInRadians()).isEqualTo(celestialBody.getAngleInRadians());

  }

  /**
   * Prueba la correcta creación de los tres planetas
   */
  @Test
  public void factoryTest() {

    CelestialBody celestialBodyV = CelestialBodyFactory.createVulcano();
    CelestialBody celestialBodyB = CelestialBodyFactory.createBetasoide();
    CelestialBody celestialBodyF = CelestialBodyFactory.createFerengi();

    assertThat(celestialBodyB.getName()).isEqualTo("Betasoide");
    assertThat(celestialBodyF.getName()).isEqualTo("Ferengi");
    assertThat(celestialBodyV.getName()).isEqualTo("Vulcano");

    SolarSystem ss = new SolarSystem(celestialBodyB,celestialBodyF,celestialBodyV);

    assertThat(ss.getCelestialBodyA()).isEqualTo(celestialBodyB);
    assertThat(ss.getCelestialBodyB()).isEqualTo(celestialBodyF);
    assertThat(ss.getCelestialBodyC()).isEqualTo(celestialBodyV);

    assertThat(ss.getCelestialBodyA().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyB().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyC().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyA().getX()).isEqualTo(2000);
    assertThat(ss.getCelestialBodyB().getX()).isEqualTo(500);
    assertThat(ss.getCelestialBodyC().getX()).isEqualTo(1000);

    ss.doRegressions(0);
    assertThat(ss.getCelestialBodyA().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyB().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyC().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyA().getX()).isEqualTo(2000);
    assertThat(ss.getCelestialBodyB().getX()).isEqualTo(500);
    assertThat(ss.getCelestialBodyC().getX()).isEqualTo(1000);

    ss.doRegressions(360);
    assertThat(ss.getCelestialBodyA().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyB().getY()).isEqualTo(0);
    assertThat(ss.getCelestialBodyC().getY()).isEqualTo(00.0);
    assertThat(ss.getCelestialBodyA().getX()).isEqualTo(2000);
    assertThat(ss.getCelestialBodyB().getX()).isEqualTo(500);
    assertThat(ss.getCelestialBodyC().getX()).isEqualTo(1000);
  }

}

package galaxy.weatherforecast.api.model;


import galaxy.weatherforecast.api.model.dto.WeatherDescriptorDTO;
import galaxy.weatherforecast.api.model.dto.WeatherForecastDTO;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.WeatherForecastCalculator;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.repository.WeatherForecastRepository;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.service.WeatherForecastService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Servicio para acceder a la persistencia
 */
public class WeatherForecastDescriptorService {

    private WeatherForecastRepository weatherForecastRepository;

    @Autowired
    private WeatherForecastCalculator weatherForecastCalculator;

    public WeatherForecastDescriptorService(
            final WeatherForecastRepository weatherForecastRepository) {
        this.weatherForecastRepository = weatherForecastRepository;
    }

    public WeatherDescriptorDTO getWeather(Integer dayNumber){
        WeatherDescriptor weatherDescriptor = this.weatherForecastRepository.findBy(dayNumber);
        return WeatherDescriptorDTO.get(dayNumber, weatherDescriptor);
    }

    public WeatherForecastDTO getForecast(Integer dayNumber){
        WeatherForecastService weatherForecastImpl = weatherForecastCalculator.calculate(dayNumber);
        return WeatherForecastDTO.get(weatherForecastImpl);
    }
}

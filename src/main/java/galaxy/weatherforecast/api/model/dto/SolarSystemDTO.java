package galaxy.weatherforecast.api.model.dto;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;

/**
 * Objeto de Datos que representa a un Sistema Solar y su información. En este caso compuesto por 3 planetas
 */
public class SolarSystemDTO {

  private CelestialBodyDTO cuerpoCelesteA;

  private CelestialBodyDTO cuerpoCelesteB;

  private CelestialBodyDTO cuerpoCelesteC;

  public SolarSystemDTO(final CelestialBodyDTO cuerpoCelesteA, final CelestialBodyDTO cuerpoCelesteB,
                        final CelestialBodyDTO cuerpoCelesteC) {
    this.cuerpoCelesteA = cuerpoCelesteA;
    this.cuerpoCelesteB = cuerpoCelesteB;
    this.cuerpoCelesteC = cuerpoCelesteC;
  }

  public static SolarSystemDTO get(final SolarSystem solarSystem) {
    return new SolarSystemDTO(CelestialBodyDTO.get(solarSystem.getCelestialBodyA()),
        CelestialBodyDTO.get(solarSystem.getCelestialBodyB()),
        CelestialBodyDTO.get(solarSystem.getCelestialBodyC()));
  }

  public CelestialBodyDTO getCuerpoCelesteA() {
    return cuerpoCelesteA;
  }

  public CelestialBodyDTO getCuerpoCelesteB() {
    return cuerpoCelesteB;
  }

  public CelestialBodyDTO getCuerpoCelesteC() {
    return cuerpoCelesteC;
  }
}

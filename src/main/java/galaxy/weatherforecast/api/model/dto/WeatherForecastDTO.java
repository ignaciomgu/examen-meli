package galaxy.weatherforecast.api.model.dto;

import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.service.WeatherForecastService;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Objeto de Datos que representa a la información perteneciente a la predicción del estado del tiempo
 */
public class WeatherForecastDTO {

  private long totalDeDiasOptimos;

  private long totalDeDiasLLuviosos;

  private long totalDeDiasSecos;

  private List<WeatherDescriptorDTO> diasMasLLuviosos;

  public static WeatherForecastDTO get(final WeatherForecastService weatherForecastImpl) {
    WeatherForecastDTO dto = new WeatherForecastDTO();
    dto.setTotalDeDiasSecos(weatherForecastImpl.getTotalDroughtDays());
    dto.setTotalDeDiasLLuviosos(weatherForecastImpl.getTotalRainyDays());
    dto.setTotalDeDiasOptimos(weatherForecastImpl.getTotalOptimalDays());

    Map<Integer, WeatherDescriptor> mostRainyDays = weatherForecastImpl.getMostRainyDays();
    LinkedList<WeatherDescriptorDTO> mostRainyDaysDto = new LinkedList<>();

    mostRainyDays.forEach(
        (day, weatherInfo) -> mostRainyDaysDto.addLast(WeatherDescriptorDTO.get(day, weatherInfo)));
    dto.setDiasMasLLuviosos(mostRainyDaysDto);
    return dto;
  }

  public long getTotalDeDiasOptimos() {
    return totalDeDiasOptimos;
  }

  public void setTotalDeDiasOptimos(final long totalDeDiasOptimos) {
    this.totalDeDiasOptimos = totalDeDiasOptimos;
  }

  public long getTotalDeDiasLLuviosos() {
    return totalDeDiasLLuviosos;
  }

  public void setTotalDeDiasLLuviosos(final long totalDeDiasLLuviosos) {
    this.totalDeDiasLLuviosos = totalDeDiasLLuviosos;
  }

  public long getTotalDeDiasSecos() {
    return totalDeDiasSecos;
  }

  public void setTotalDeDiasSecos(final long totalDeDiasSecos) {
    this.totalDeDiasSecos = totalDeDiasSecos;
  }

  public List<WeatherDescriptorDTO> getDiasMasLLuviosos() {
    return diasMasLLuviosos;
  }

  public void setDiasMasLLuviosos(
      final List<WeatherDescriptorDTO> diasMasLLuviosos) {
    this.diasMasLLuviosos = diasMasLLuviosos;
  }

}
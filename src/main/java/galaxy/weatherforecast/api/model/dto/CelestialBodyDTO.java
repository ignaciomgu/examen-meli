package galaxy.weatherforecast.api.model.dto;

import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBody;

import java.awt.geom.Point2D;

/**
 * Objeto de Datos que representa a un Cuerpo Celesto y su información
 */
public class CelestialBodyDTO {

  private String nombre;

  private long grados;

  private Point2D posicion;

  public CelestialBodyDTO(final String nombre, final long grados, final Point2D posicion) {
    this.nombre = nombre;
    this.grados = grados;
    this.posicion = posicion;
  }

  public static CelestialBodyDTO get(final CelestialBody celestialBody) {
    return new CelestialBodyDTO(celestialBody.getName(), celestialBody.getAngleInDegrees(), celestialBody.getPosition());
  }

  public String getNombre() {
    return nombre;
  }

  public long getGrados() {
    return grados;
  }

  public Point2D getPosicion() {
    return posicion;
  }
}

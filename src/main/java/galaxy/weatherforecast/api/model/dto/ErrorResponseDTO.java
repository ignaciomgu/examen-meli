package galaxy.weatherforecast.api.model.dto;


public class ErrorResponseDTO {
    private String messaje;
    public ErrorResponseDTO(String messaje) {
        this.messaje = messaje;
    }

    public String getMessaje() {
        return messaje;
    }

    public void setMessaje(String messaje) {
        this.messaje = messaje;
    }
}

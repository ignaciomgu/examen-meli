package galaxy.weatherforecast.api.model.dto;

import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherName;

/**
 * Objeto de Datos que representa a un la descripción del tiempo.
 */
public class WeatherDescriptorDTO {

  private int dia;

  private WeatherName nombre;

  private double lluvia;

  private SolarSystemDTO sistemaSolarDTO;

  public WeatherDescriptorDTO(final int dia, final WeatherName nombre, final double lluvia,
                              final SolarSystemDTO sistemaSolarDTO) {
    this.dia = dia;
    this.nombre = nombre;
    this.lluvia = lluvia;
    this.sistemaSolarDTO = sistemaSolarDTO;
  }

  public static WeatherDescriptorDTO get(final int day, WeatherDescriptor weatherDescriptor) {
    return new WeatherDescriptorDTO(day, weatherDescriptor.getName(),
        weatherDescriptor.getRain(), SolarSystemDTO.get(weatherDescriptor.getSolarSystem()));
  }

  public int getDia() {
    return dia;
  }

  public WeatherName getNombre() {
    return nombre;
  }

  public double getLluvia() {
    return lluvia;
  }

  public SolarSystemDTO getSistemaSolarDTO() {
    return sistemaSolarDTO;
  }
}

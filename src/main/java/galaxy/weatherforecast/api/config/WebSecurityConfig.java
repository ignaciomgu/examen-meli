package galaxy.weatherforecast.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configuración de seguridad framework spring security
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Se concede permisos para unicamente usuarios autenticados para la interfaz general.
     * Se concede permisos los recursos "climna" y "pronostico" a los usuarios con Rol "USER" (unico rol por ahora)
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.httpBasic().and().csrf().disable().authorizeRequests().antMatchers(HttpMethod.GET,"/api").authenticated()
                .antMatchers(HttpMethod.GET,"/clima").hasRole("USER")
                .antMatchers(HttpMethod.GET,"/pronostico").hasRole("USER");


    }

    /**
     * Demostración de login con usuarios para la API REST.
     * En este metodo están utilizados los usuarios de forma sencilla en memoria.
     * @param auth
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("meli").password("{noop}iamin").roles("USER").and()
                .withUser("mercadolibre").password("{noop}iampartofthestaff").roles("USER");
    }




}



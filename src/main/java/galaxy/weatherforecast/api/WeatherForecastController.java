package galaxy.weatherforecast.api;

import galaxy.weatherforecast.api.model.WeatherForecastDescriptorService;
import galaxy.weatherforecast.api.model.dto.ErrorResponseDTO;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.WeatherForecastCalculator;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.repository.WeatherForecastRepository;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * Interfaz Web REST
 */
@RestController
@RequestMapping("/api")
public class WeatherForecastController {

    @Autowired
    private WeatherForecastDescriptorService weatherForecastDescriptorService;

    private static final Logger logger = LoggerFactory.getLogger(WeatherForecastController.class);

  /**
   * Pronostico total del tiempo en la cantidad de días pasada por parametro
   */
  @RequestMapping(value = "/pronostico", method = RequestMethod.GET, produces = "application/json")
  @Validated
  public ResponseEntity calculateForcast(@ApiParam(value = "Cantidad de días a predecir", allowableValues = "range[1,3650]", required = true) @RequestParam("dia") @NotNull String days)
      throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    logger.info("Usuario: "+auth.getName() + " - Petición del Pronostico");
    Integer dayNumber;
    try {
      dayNumber = Integer.parseInt(days);
      if (dayNumber < 1 || dayNumber > 3650) {
          ErrorResponseDTO error = new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650");
          logger.error("Usuario: "+auth.getName() + " - Error en petición del Pronostico: "+error.getMessaje());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
      }
    }catch(NumberFormatException e){
        ErrorResponseDTO error = new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650");
        logger.error("Usuario: "+auth.getName() + " - Error en petición del Pronostico: "+error.getMessaje());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    return ResponseEntity.ok(weatherForecastDescriptorService.getForecast(dayNumber));
  }

  /**
   * Como está el tiempo en ese momento determinado pasado por parametro
   */
  @RequestMapping(value = "/clima", method = RequestMethod.GET, produces = "application/json")
  @Validated
  public ResponseEntity calculateWeatherDay(@ApiParam(value = "Día a diagnosticar el clima", allowableValues = "range[1,3650]", required = true) @RequestParam("dia") @NotNull String day)
      throws Exception {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      logger.info("Usuario: "+auth.getName() + " - Petición del Clima");
      Integer dayNumber;
        try {
          dayNumber = Integer.parseInt(day);
          if (dayNumber < 1 || dayNumber > 3650) {
            ErrorResponseDTO error = new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650");
            logger.error("Usuario: "+auth.getName() + " - Error en petición del Clima: "+error.getMessaje());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
          }
        }catch(NumberFormatException e){
          ErrorResponseDTO error = new ErrorResponseDTO("El parámetro debe poseer un valor entre 1 y 3650");
          logger.error("Usuario: "+auth.getName() + " - Error en petición del Clima: "+error.getMessaje());
          return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }

    return ResponseEntity.ok(weatherForecastDescriptorService.getWeather(dayNumber));
  }


}

package galaxy.weatherforecast.businesslogic.weatherforecast;

import galaxy.weatherforecast.api.model.WeatherForecastDescriptorService;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherCalculator;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.WeatherForecastCalculator;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.repository.WeatherForecastRepository;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.service.WeatherForecastServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Inicializador del sistema solar
 */
@Configuration
public class WeatherForecastInit {

  @Autowired
  private SolarSystem solarSystem;

  @Autowired
  private WeatherCalculator weatherCalculator;

  private static final Logger logger = LoggerFactory.getLogger(WeatherForecastServiceImpl.class);

  @Bean
  public WeatherForecastRepository forecastRepository() {
    logger.info("Obtención del Pronostico");
    return new WeatherForecastRepository(solarSystem, weatherCalculator);
  }

  @Bean
  public WeatherForecastCalculator calculateForecast() {
    logger.info("Calculo del Clima");
    return new WeatherForecastCalculator(forecastRepository());
  }

  @Bean
  public WeatherForecastDescriptorService describeForecast() {
    logger.info("Descripción del Pronostico");
    return new WeatherForecastDescriptorService(forecastRepository());
  }
}

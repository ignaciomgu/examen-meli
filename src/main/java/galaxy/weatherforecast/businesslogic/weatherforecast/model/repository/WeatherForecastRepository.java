package galaxy.weatherforecast.businesslogic.weatherforecast.model.repository;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherCalculator;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Persistencia del pronóstico del tiempo
 */
public class WeatherForecastRepository {

  private static final Logger logger = LoggerFactory.getLogger(WeatherForecastRepository.class);

  private SolarSystem solarSystem;

  private WeatherCalculator weatherCalculator;

  private Map<Integer, WeatherDescriptor> cache = new HashMap<>(365);

  public WeatherForecastRepository(final SolarSystem solarSystem,
                                   final WeatherCalculator weatherCalculator) {

    this.solarSystem = solarSystem;
    this.weatherCalculator = weatherCalculator;
    initCache();
  }

  private void initCache() {
    logger.info("Inicializando Cache");
    for (int day = 1; day <= 365; day++) {
      this.solarSystem.doRegressions(day);
      cache.put(day, this.weatherCalculator.calculate(solarSystem));
    }
  }

  public Map<Integer, WeatherDescriptor> findAll(final int days) {
    logger.info("Obtención de todos los Descriptores de Clima en: "+days+" dias");
    Map<Integer, WeatherDescriptor> forecast = new LinkedHashMap<>(days);
    for (int i = 1; i <= days; i++) {
      forecast.put(i, getFromCache(i));
    }
    return forecast;
  }

  public WeatherDescriptor findBy(final int day) {
    logger.info("Obtención del Descriptor de Clima en el dia: "+day);
    return getFromCache(day);
  }

  private WeatherDescriptor getFromCache(final int day) {
    logger.info("Obtención del Descriptor de Clima de la cache en el dia: "+day);
    if (day % 365 != 0) {
      return cache.get(day % 365);
    } else {
      return cache.get(365);
    }
  }

}

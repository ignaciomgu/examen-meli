package galaxy.weatherforecast.businesslogic.weatherforecast.model.service;

import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

/**
 * Implementación del servicio de obtención de información del clima persistida
 */
public class WeatherForecastServiceImpl implements WeatherForecastService{

  private static final Logger logger = LoggerFactory.getLogger(WeatherForecastServiceImpl.class);

  private Map<Integer, WeatherDescriptor> forecast;

  public WeatherForecastServiceImpl(final Map<Integer, WeatherDescriptor> forecast) {
    this.forecast = forecast;
  }

  @Override
  public long getTotalOptimalDays() {
    return getTotalDaysByType(WeatherName.OPTIMAL);
  }

  @Override
  public long getTotalRainyDays() {
    return getTotalDaysByType(WeatherName.RAIN);
  }

  @Override
  public long getTotalDroughtDays() {
    return getTotalDaysByType(WeatherName.DROUGHT);
  }

  @Override
  public Map<Integer, WeatherDescriptor> getMostRainyDays() {
    logger.info("Obtención de días mas lluviosos");
    Stream<Entry<Integer, WeatherDescriptor>> collected = forecast.entrySet().stream()
        .filter(integerWeatherInfoEntry
            -> WeatherName.RAIN.equals(integerWeatherInfoEntry.getValue().getName()))
        .sorted(Entry.comparingByValue(
            Comparator.comparingDouble(WeatherDescriptor::getRain).reversed()))
        .limit(50);
    Map<Integer, WeatherDescriptor> mostRainyDays = new LinkedHashMap<>(10);
    collected.forEach(e -> mostRainyDays.put(e.getKey(), e.getValue()));
    return mostRainyDays;
  }

  @Override
   public long getTotalDaysByType(final WeatherName weatherName) {
    logger.info("Obtención del total de días de los dias de tipo: "+weatherName.name());
    return forecast.values().stream()
        .filter(weatherInfo -> weatherName.equals(weatherInfo.getName())).count();
  }
}

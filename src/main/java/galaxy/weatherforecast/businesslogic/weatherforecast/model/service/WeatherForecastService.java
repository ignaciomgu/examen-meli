package galaxy.weatherforecast.businesslogic.weatherforecast.model.service;


import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherName;

import java.util.Map;

/**
 * Servicio de obtenciòn de informaón persistida del clima
 */
public interface WeatherForecastService {

    public long getTotalOptimalDays();

    public long getTotalRainyDays();

    public long getTotalDroughtDays();

    public Map<Integer, WeatherDescriptor> getMostRainyDays();

    public long getTotalDaysByType(final WeatherName weatherName);

}

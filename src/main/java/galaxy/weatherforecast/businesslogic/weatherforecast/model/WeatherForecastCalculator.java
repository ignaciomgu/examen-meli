package galaxy.weatherforecast.businesslogic.weatherforecast.model;

import galaxy.weatherforecast.businesslogic.weatherforecast.model.repository.WeatherForecastRepository;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.service.WeatherForecastServiceImpl;
import galaxy.weatherforecast.businesslogic.weatherforecast.model.service.WeatherForecastService;


/**
 * Interactua con repository para calcular el pronostico a cierta cantidad de dias
 */
public class WeatherForecastCalculator {

  private WeatherForecastRepository weatherForecastRepository;

  public WeatherForecastCalculator(
      final WeatherForecastRepository weatherForecastRepository) {
    this.weatherForecastRepository = weatherForecastRepository;
  }

  public WeatherForecastService calculate(final int days) {
    return new WeatherForecastServiceImpl(weatherForecastRepository.findAll(days));
  }
}

package galaxy.weatherforecast.businesslogic.celestialbody.model;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modelo de datos que representa un planeta
 */
public class CelestialBody {

  private String name;

  //Velocidad Angular
  private int velocityInDegreesPerDay;

  private double sunDistanceInKm;

  private long angleInDegrees;

  private double angleInRadians;

  private Point2D position;

  private static final Logger logger = LoggerFactory.getLogger(CelestialBody.class);

  public CelestialBody(final String name, final int velocityInDegreesPerDay, final double sunDistanceInKm) {
    this.name = name;
    this.velocityInDegreesPerDay = velocityInDegreesPerDay;
    this.sunDistanceInKm = sunDistanceInKm;
    this.position = new Double(sunDistanceInKm, 0.0);
  }

  public CelestialBody getClone() {
    logger.info("Clonando Planeta "+this.name);
    CelestialBody celestialBody = new CelestialBody(name, velocityInDegreesPerDay, sunDistanceInKm);
    celestialBody.angleInDegrees = this.angleInDegrees;
    celestialBody.angleInRadians = this.angleInRadians;
    celestialBody.position = new Double(position.getX(), position.getY());
    return celestialBody;
  }

  /**
   * Realiza una regresión de la posición que tendrá de acuerdo al movimiento el día pasado por parametro
   * @param days el día que se quiere regresionar
   */
  public void doRegression(final int days) {
    angleInDegrees = (velocityInDegreesPerDay * days) % 360;
    if (velocityInDegreesPerDay < 0) {
      angleInDegrees = 360 + angleInDegrees;
    }
    angleInRadians = Math.toRadians(angleInDegrees);

    double xAxis = Precision.round(Math.cos(angleInRadians) * sunDistanceInKm,0);
    double yAxis = Precision.round(Math.sin(angleInRadians) * sunDistanceInKm,0);
    xAxis = xAxis==-0.0?0.0:xAxis;
    yAxis = yAxis==-0.0?0.0:yAxis;
    this.position = new Double(xAxis, yAxis);
    logger.info("Regresionando Planeta "+this.name+" en "+days+" días");
    logger.info("X: "+xAxis+" Y: "+yAxis);
  }

  public double getAngleInRadians() {
    return angleInRadians;
  }

  public long getAngleInDegrees() {
    return angleInDegrees;
  }

  public Point2D getPosition() {
    return position;
  }

  public double getX() {
    return this.position.getX();
  }

  public double getY() {
    return this.position.getY();
  }

  public String getName() {
    return name;
  }


}

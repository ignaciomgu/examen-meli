package galaxy.weatherforecast.businesslogic.celestialbody.model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creador de los planetas del sistema solar
 */
public class CelestialBodyFactory {

  private static final Logger logger = LoggerFactory.getLogger(CelestialBodyFactory.class);

  public static CelestialBody createFerengi() {
    logger.info("Creación del Planeta Ferengi");
    return new CelestialBody("Ferengi", 1, 500);
  }

  public static CelestialBody createBetasoide() {
    logger.info("Creación del Planeta Betasoide");
    return new CelestialBody("Betasoide", 3, 2000);
  }

  public static CelestialBody createVulcano() {
    logger.info("Creación del Planeta Vulcano");
    return new CelestialBody("Vulcano", -5, 1000);
  }

}

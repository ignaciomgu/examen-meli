package galaxy.weatherforecast.businesslogic.celestialbody.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Modelo de datos que representa al sistema solar (en este caso con 3 planetas)
 */
public class SolarSystem {

  private CelestialBody celestialBodyA;

  private CelestialBody celestialBodyB;

  private CelestialBody celestialBodyC;

  private static final Logger logger = LoggerFactory.getLogger(SolarSystem.class);


  public SolarSystem(final CelestialBody celestialBodyA, final CelestialBody celestialBodyB, final CelestialBody celestialBodyC) {
    this.celestialBodyA = celestialBodyA;
    this.celestialBodyB = celestialBodyB;
    this.celestialBodyC = celestialBodyC;
  }

  public SolarSystem getClone() {
    logger.info("Clonando Sistema Solar ");
    return new SolarSystem(celestialBodyA.getClone(), celestialBodyB.getClone(), celestialBodyC.getClone());
  }

  public CelestialBody getCelestialBodyA() {
    return celestialBodyA;
  }

  public CelestialBody getCelestialBodyB() {
    return celestialBodyB;
  }

  public CelestialBody getCelestialBodyC() {
    return celestialBodyC;
  }

  public void doRegressions(final int days) {
    logger.info("Regresionando Planetas del Sistema Solar en "+days+" días");
    this.celestialBodyA.doRegression(days);
    this.celestialBodyB.doRegression(days);
    this.celestialBodyC.doRegression(days);
  }

}

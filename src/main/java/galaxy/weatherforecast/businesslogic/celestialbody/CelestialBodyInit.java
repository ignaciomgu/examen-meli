package galaxy.weatherforecast.businesslogic.celestialbody;

import galaxy.weatherforecast.api.WeatherForecastController;
import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBodyFactory;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Inicializador del sistema solar con los tres planetas.
 */
@Configuration
public class CelestialBodyInit {
  private static final Logger logger = LoggerFactory.getLogger(CelestialBodyInit.class);
  @Bean
  public SolarSystem solarSystem() {
    logger.info("Creación del Sistema Solar");
    return new SolarSystem(
        CelestialBodyFactory.createBetasoide(),
        CelestialBodyFactory.createFerengi(),
        CelestialBodyFactory.createVulcano());
  }

}

package galaxy.weatherforecast.businesslogic.forecastengine;

import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.DroughtDayProcessor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.OptimalDayProcessor;
import java.util.Arrays;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.RainyDayProcessor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Inicializador del motor de calculo de pronóstico del clima del sistema solar
 */
@Configuration
public class EngineInit {

  private static final Logger logger = LoggerFactory.getLogger(EngineInit.class);

  @Bean
  public WeatherCalculator weatherCalculator() {
    logger.info("Creación de la Calculadora de Climas");
    return new WeatherCalculator(Arrays.asList(
        new OptimalDayProcessor(),
        new RainyDayProcessor(),
        new DroughtDayProcessor()));
  }
}

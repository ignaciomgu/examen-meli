package galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherName;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Objeto que representa al calculo de los dias que no poseen nombre, es decir, cuando el día no es lluvioso, ni seco ni soleadp
 */
public class NoNameProcessor implements WeatherProcessor {

  private static final Logger logger = LoggerFactory.getLogger(NoNameProcessor.class);

  @Override
  public WeatherDescriptor create(final SolarSystem solarSystem) {
    logger.info("Creación del Descriptor de Clima Sin Nombre");
    return new WeatherDescriptor(solarSystem, WeatherName.NO_NAME, 0);
  }

  @Override
  public boolean isOccurring(final SolarSystem solarSystem) {
    logger.info("Verificando Ocurrencia de Clima Sin Nombre");
    return false;
  }
}

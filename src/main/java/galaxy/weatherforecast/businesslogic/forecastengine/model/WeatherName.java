package galaxy.weatherforecast.businesslogic.forecastengine.model;

/**
 * Tipos de climas
 */
public enum WeatherName {
  DROUGHT, RAIN, OPTIMAL, NO_NAME
}

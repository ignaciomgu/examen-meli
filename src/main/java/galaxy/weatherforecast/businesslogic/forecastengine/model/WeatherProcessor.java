package galaxy.weatherforecast.businesslogic.forecastengine.model;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;

/**
 * Contrato para calculo de clima
 */
public interface WeatherProcessor {

  WeatherDescriptor create(final SolarSystem solarSystem);

  boolean isOccurring(final SolarSystem solarSystem);

}

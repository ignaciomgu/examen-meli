package galaxy.weatherforecast.businesslogic.forecastengine.model;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine.NoNameProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Calculadora de pronostico del tiempo
 */
public class WeatherCalculator {

  private List<WeatherProcessor> processor;
  private static final Logger logger = LoggerFactory.getLogger(WeatherCalculator.class);
  public WeatherCalculator(final List<WeatherProcessor> processor) {
    this.processor = processor;
  }

  public WeatherDescriptor calculate(final SolarSystem solarSystem) {
    logger.info("Obtención del Descriptor de Clima");
    return processor.stream()
        .filter(weatherResolver -> weatherResolver.isOccurring(solarSystem))
        .findFirst()
        .orElse(new NoNameProcessor())
        .create(solarSystem);
  }
}

package galaxy.weatherforecast.businesslogic.forecastengine.model;

/**
 * La calculadora posee un algoritmo que debe detectar que los tres cuerpos celestes estén alineados entre sí y que además estén y
 * NO estén alineados con el Sol.
 * Los puntos A(x,y), B(v,h) y C(f,g) están alineados, si y solo si, los vectores AB y BC tienen la misma dirección, y esto se
 * da cuando sus coordenadas son proporcionales:
 * (v-x)     (h-y)
 * ------ = -------
 * (f-v)     (g-h)
 * No obstante, para que éstos no estén alineados con el Sol, basta tomar dos de estos cuerpos celestes y realizar el mismo calculo:
 * A(x,y), B(v,h) y SOL(0,0)
 * Calculo:
 * (v-x)     (h-y)
 * ------ = -------
 * (0-v)     (0-h)
 * Con el fin de eliminar la posibilidad de dividir entre ceros se realiza una manipulación algebráica para la detección de alineamiento
 * entre tres cuerpos celestes o dos cuerpos celestes y el sol [(y3-y1)*(x2-x1) = (y2-y1)*(x3 -x1)]:
 * (g-y)*(v-x) = (h-y)*(f -x)
 *
 */
public class AlignmentCalculator {

    // Indice de tolerancia de alineamiento
    public static int indexOfTolerance = 2;

    /**
     * Prueba que tres puntos pasados por parámetros esten alineados entre si pero no con el sol
     * @param x (valor sobre el eje X del punto 1 a comparar)
     * @param y (valor sobre el eje X del punto 1 a comparar)
     * @param v (valor sobre el eje X del punto 2 a comparar)
     * @param h (valor sobre el eje Y del punto 2 a comparar)
     * @param f (valor sobre el eje X del punto 3 a comparar)
     * @param g (valor sobre el eje Y del punto 3 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados entre sí y NO con el sol)
     */
    public static boolean areAlignedButNotWithTheSun(double x, double y, double v, double h, double f, double g){
        return ((g-y)*(v-x) == (h-y)*(f -x) && !areAlignWithTheSun(v, h, f, g));
    }

    /**
     * Método igual al areAlignedButNotWithTheSun, pero con indice de tolerancia
     * @param x (valor sobre el eje X del punto 1 a comparar)
     * @param y (valor sobre el eje X del punto 1 a comparar)
     * @param v (valor sobre el eje X del punto 2 a comparar)
     * @param h (valor sobre el eje Y del punto 2 a comparar)
     * @param f (valor sobre el eje X del punto 3 a comparar)
     * @param g (valor sobre el eje Y del punto 3 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados entre sí y NO con el sol)
     */
    public static boolean areAlignedButNotWithTheSunWithTolerance(double x, double y, double v, double h, double f, double g){
        double result = (g-y)*(v-x) - (h-y)*(f -x);
        return (((result < indexOfTolerance && result > -indexOfTolerance) || result == indexOfTolerance) && !areAlignWithTheSunWithTolerance(v, h, f, g));
    }

    /**
     * Prueba que los tres puntos pasados por parametros estén alineados entre si y con el sol
     * @param x (valor sobre el eje X del punto 1 a comparar)
     * @param y (valor sobre el eje X del punto 1 a comparar)
     * @param v (valor sobre el eje X del punto 2 a comparar)
     * @param h (valor sobre el eje Y del punto 2 a comparar)
     * @param f (valor sobre el eje X del punto 3 a comparar)
     * @param g (valor sobre el eje Y del punto 3 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados entre sí y con el sol)
     */
    public static boolean areAlignedAndWithTheSun(double x, double y, double v, double h, double f, double g){
        return ((g-y)*(v-x) == (h-y)*(f -x) && areAlignWithTheSun(v, h, f, g));
    }

    /**
     * Método igual al areAlignedAndWithTheSun, pero con indice de tolerancia
     * @param x (valor sobre el eje X del punto 1 a comparar)
     * @param y (valor sobre el eje X del punto 1 a comparar)
     * @param v (valor sobre el eje X del punto 2 a comparar)
     * @param h (valor sobre el eje Y del punto 2 a comparar)
     * @param f (valor sobre el eje X del punto 3 a comparar)
     * @param g (valor sobre el eje Y del punto 3 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados entre sí y con el sol)
     */
    public static boolean areAlignedAndWithTheSunWithTolerance(double x, double y, double v, double h, double f, double g){
        double result = (g-y)*(v-x) - (h-y)*(f -x);
        return (((result < indexOfTolerance && result > -indexOfTolerance) || result == indexOfTolerance)  && areAlignWithTheSunWithTolerance(v, h, f, g));
    }

    /**
     * Prueba que los dos puntos pasados por parametros estén alineados al sol
     * @param v (valor sobre el eje X del punto 1 a comparar)
     * @param h (valor sobre el eje Y del punto 1 a comparar)
     * @param f (valor sobre el eje X del punto 2 a comparar)
     * @param g (valor sobre el eje Y del punto 2 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados con el sol)
     */
    private static boolean areAlignWithTheSun(double v, double h, double f, double g){
        // Coordenadas del Sol
        double x = 0.0;
        double y = 0.0;

        return (g-y)*(v-x) == (h-y)*(f -x);
    }

    /**
     * Método igual al areAlignWithTheSun, pero con indice de tolerancia
     * @param v (valor sobre el eje X del punto 1 a comparar)
     * @param h (valor sobre el eje Y del punto 1 a comparar)
     * @param f (valor sobre el eje X del punto 2 a comparar)
     * @param g (valor sobre el eje Y del punto 2 a comparar)
     * @return true (si es que los puntos pasados por parametros se encuentran alineados con el sol)
     */
    private static boolean areAlignWithTheSunWithTolerance(double v, double h, double f, double g){
        // Coordenadas del Sol
        double x = 0.0;
        double y = 0.0;
        double result = (g-y)*(v-x) - (h-y)*(f -x);
        return (((result < indexOfTolerance && result > -indexOfTolerance) || result == indexOfTolerance)  || result == indexOfTolerance);
    }
}

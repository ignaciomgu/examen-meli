package galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine;

import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBody;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.AlignmentCalculator;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Objeto que representa al calculo de los dias optimos, dentro del mismo se encuentra la utilización de los
 * diferentes algoritmos geometricos para poder obtener el resultado del clima de acuerdo a las diferentes posiciones
 * de los planetas. En este caso particular, para que un dìa sea Optimo, los tres planetas deben estar alineados entre si,
 * pero a su vez, no deben estar alineados al sol.
 */
public class OptimalDayProcessor implements WeatherProcessor {

  private static final Logger logger = LoggerFactory.getLogger(OptimalDayProcessor.class);

  @Override
  public WeatherDescriptor create(final SolarSystem solarSystem) {
    logger.info("Creación del Descriptor de Clima Optimo");
    return WeatherDescriptor.createOptimal(solarSystem);
  }

  /**
   * Para que este perído ocurra el algoritmo debe detectar que los tres cuerpos celestes estén alineados entre sí y que además
   * NO estén alineados con el Sol.
   *
   * @param solarSystem (que contiene los cuerpos celestes)
   * @return true (si es que se cumple la condición)
   */
  @Override
  public boolean isOccurring(final SolarSystem solarSystem) {
    logger.info("Verificando Ocurrencia de Clima Optimo");

    CelestialBody celestialBodyA = solarSystem.getCelestialBodyA();
    CelestialBody celestialBodyB = solarSystem.getCelestialBodyB();
    CelestialBody celestialBodyC = solarSystem.getCelestialBodyC();

    double x = celestialBodyA.getX();
    double y = celestialBodyA.getY();
    double v = celestialBodyB.getX();
    double h = celestialBodyB.getY();
    double f = celestialBodyC.getX();
    double g = celestialBodyC.getY();

    return AlignmentCalculator.areAlignedButNotWithTheSun(x, y, v, h, f, g);
  }

}

package galaxy.weatherforecast.businesslogic.forecastengine.model.ProcessingEngine;

import galaxy.weatherforecast.businesslogic.celestialbody.model.CelestialBody;
import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherDescriptor;
import galaxy.weatherforecast.businesslogic.forecastengine.model.WeatherProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Objeto que representa al calculo de los dias lluviosos, dentro del mismo se encuentra la utilización de los
 * diferentes algoritmos geometricos para poder obtener el resultado del clima de acuerdo a las diferentes posiciones
 * de los planetas. En este caso particular, para que un dia sea lluvioso, el sol debe estar dentro del perimetro
 * del triangulo que forman los otros tres planetas
 */
public class RainyDayProcessor implements WeatherProcessor {

  private static final Logger logger = LoggerFactory.getLogger(RainyDayProcessor.class);

  @Override
  public WeatherDescriptor create(final SolarSystem solarSystem) {
    logger.info("Creación del Descriptor de Clima Lluvioso");
    return WeatherDescriptor
        .createRain(solarSystem, getRain(solarSystem));
  }

  /**
   * Para que este perído ocurra el algoritmo debe detectar que los tres cuerpos celestes estén formando un triangulo y
   * que el Sol se encuentro dentro de esa área comprendida por ese poligono
   * La orientación de un triángulo es la misma que la orientación de sus tres vértices, así que se puede establecer un algoritmo
   * sencillo para decidir si un punto está o no en el interior de un triángulo.
   * 1) - Considerando el triángulo A(x,y), B(v,h) y C(f,g) y el punto P(0.0), el algoritmo quedaría:
   *
   * (x -f) * (h - g) - (y - g) * (v - f)
   *
   * Si el resultado es mayor o igual que 0, la orientación del triángulo será positiva. En caso contrario, la orientación del triángulo será negativa.
   * 2) - Ahora se calcula la orientación de los triángulos que forma el punto P con los vértices del triángulo A(x,y), B(v,h) y C(f,g).
   * Se calcula la orientación de los triángulos [A(x,y), B(v,h) y P(0.0)], [B(v,h), C(f,g) y P(0.0)], [A(x,y), C(f,g) y P(0.0)], con el método explicado en el punto 1).
   *
   * En el caso de que la orientación del triángulo A(x,y), B(v,h) y C(f,g) sea positiva:
   * Si las orientaciones de los tres triángulos que tienen como vértice el punto P, calculadas en el punto 2, son positivas el punto está dentro del triángulo.
   * En caso contrario el punto está situado fuera del triángulo.
   *
   * En el caso de que la orientación del triángulo A(x,y), B(v,h) y C(f,g) sea negativa:
   * Si las orientaciones de los tres triángulos que triángulos que tienen como vértice el punto P son negativas, el punto está dentro del triángulo
   * En caso contrario el punto está situado fuera del triángulo.
   *
   * @param solarSystem (que contiene los cuerpos celestes)
   * @return true (si es que se cumple la condición)
   */
  @Override
  public boolean isOccurring(final SolarSystem solarSystem) {
    logger.info("Verificando Ocurrencia del Clima Lluvioso");

    CelestialBody celestialBodyA = solarSystem.getCelestialBodyA();
    CelestialBody celestialBodyB = solarSystem.getCelestialBodyB();
    CelestialBody celestialBodyC = solarSystem.getCelestialBodyC();

    double x = celestialBodyA.getX();
    double y = celestialBodyA.getY();
    double v = celestialBodyB.getX();
    double h = celestialBodyB.getY();
    double f = celestialBodyC.getX();
    double g = celestialBodyC.getY();

    if(this.isPositive(x, y, v, h, f, g)){
      return this.isPositive(x, y, v, h, 0.0, 0.0) && this.isPositive(0.0, 0.0, v, h, f, g) && this.isPositive(x, y, 0.0, 0.0, f, g);
    }else{
      return !(this.isPositive(x, y, v, h, 0.0, 0.0) && this.isPositive(0.0, 0.0, v, h, f, g) && this.isPositive(x, y, 0.0, 0.0, f, g));
    }

  }

  private boolean isPositive(double x, double y, double v, double h, double f, double g){
    return ((x -f) * (h - g) - (y - g) * (v - f)) >= 0;
  }


  /**
   * Para obtener el indice de lluvia calcula la distancia de los puntos
   * @param solarSystem
   * @return indice de lluvia
   */
  private double getRain(final SolarSystem solarSystem) {
    logger.info("Obtención de indice de lluevia");

    CelestialBody celestialBody1 = solarSystem.getCelestialBodyA();
    CelestialBody celestialBody2 = solarSystem.getCelestialBodyB();
    CelestialBody celestialBody3 = solarSystem.getCelestialBodyC();

    return celestialBody1.getPosition().distance(celestialBody2.getPosition())
            + celestialBody2.getPosition().distance(celestialBody3.getPosition())
            + celestialBody3.getPosition().distance(celestialBody1.getPosition());
  }


}

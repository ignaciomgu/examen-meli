package galaxy.weatherforecast.businesslogic.forecastengine.model;

import galaxy.weatherforecast.businesslogic.celestialbody.model.SolarSystem;

/**
 * Objeto que describe la totalidad de la información sobre un clima determinado
 */
public class WeatherDescriptor {

  private SolarSystem solarSystem;

  private WeatherName name;
  //Indice de lluvia
  private double rain;

  public WeatherDescriptor(final SolarSystem solarSystem,
                           final WeatherName name, final double rain) {
    this.solarSystem = solarSystem.getClone();
    this.name = name;
    this.rain = rain;
  }

  public static WeatherDescriptor createOptimal(final SolarSystem solarSystem) {
    return new WeatherDescriptor(solarSystem, WeatherName.OPTIMAL, 0.0);
  }

  public static WeatherDescriptor createDrought(final SolarSystem solarSystem) {
    return new WeatherDescriptor(solarSystem, WeatherName.DROUGHT, 0.0);
  }

  public static WeatherDescriptor createRain(final SolarSystem solarSystem,
                                             final double precipitation) {
    return new WeatherDescriptor(solarSystem, WeatherName.RAIN, precipitation);
  }

  public boolean isOptimalDay() {
    return WeatherName.OPTIMAL.equals(name);
  }

  public boolean isDroughtDay() {
    return WeatherName.DROUGHT.equals(name);
  }

  public boolean isRainyDay() {
    return WeatherName.RAIN.equals(name);
  }

  public WeatherName getName() {
    return name;
  }

  public SolarSystem getSolarSystem() {
    return solarSystem;
  }

  public double getRain() {
    return rain;
  }

}

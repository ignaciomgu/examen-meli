package galaxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Inicia la Aplicación
 */
@SpringBootApplication
public class MeliGalaxyApplication {

  public static void main(String[] args) {
    SpringApplication.run(MeliGalaxyApplication.class, args);
  }
}
